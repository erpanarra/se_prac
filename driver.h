/*
 * driver.h
 *
 *  Created on: 18 nov. 2017
 *      Author: Antonio
 */

#ifndef DRIVER_H_
#define DRIVER_H_
#define NUM_ADC_CHANNELS 3

#include <stdint.h>

typedef struct captura_Acelerometro
{
 float EjeX;
 float EjeY;
 float EjeZ;
}captura_A;


extern void init_Acelerometro(captura_A *);

extern void lee_Acelerometro(captura_A *, float );


extern void miftoa(float,char *,int);
#endif /* DRIVER_H_ */
