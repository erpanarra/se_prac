//*****************************************************************************
//
// Copyright (C) 2015 - 2016 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//
//  Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the
//  distribution.
//
//  Neither the name of Texas Instruments Incorporated nor the names of
//  its contributors may be used to endorse or promote products derived
//  from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>

// Includes FreeRTOS
#include "FreeRTOS.h"
#include "task.h"


#include "queue.h"
#include "semphr.h"

// Includes drivers
#include "i2c_driver.h"
#include "temp_sensor.h"
#include "opt3001.h"
#include "tmp006.h"

#include <ti/grlib/grlib.h>
#include "LcdDriver/Crystalfontz128x128_ST7735.h"

#include "driver.h"

// Definicion de parametros RTOS
#define TEMP_TASK_PRIORITY  3
#define ACEL_TASK_PRIORITY  3
#define LCD_TASK_PRIORITY    1
#define READER_TASK_PRIORITY  1
#define HEARTBEAT_TASK_PRIORITY 1
#define QUEUE_SIZE  10
#define ONE_SECOND_MS  1000
#define HALF_SECOND_MS  500
#define HEART_BEAT_ON_MS 10
#define HEART_BEAT_OFF_MS 990
#define BUFFER_SIZE 10


// Prototipos de funciones privadas
static void prvSetupHardware(void);
static void prvTempLuxTask(void *pvParameters);
static void prvReaderTask(void *pvParameters);
static void prvObtenG(void *pvParameters);
static void prvEscribeLCD(void *pvParameters);
static void prvHeartBeatTask(void *pvParameters);


void InicializaColas(void);
// Declaracion de un mutex para acceso unico a I2C, UART y buffers
SemaphoreHandle_t xMutexI2C;
SemaphoreHandle_t xTomatemp;


/* Graphic library context */
Graphics_Context g_sContext;

typedef enum Sensor
{
    light = 1,
    temp = 2
} Sensor;



// Queue element
typedef struct {
    Sensor sensor;
    float value;
} Queue_reg_t;


QueueHandle_t xQueue_Acel;
QueueHandle_t xQueue_Temp_Lux;

captura_A Medida_Acc;

float mediaX=0.0;
float mediaY=0.0;
float mediaZ=0.0;
float mediaLux=0.0;

float valoresTemp, valorLux,diferencia;
float MediaTemp,MediaLux;
float valoresX,valoresY,valoresZ;



float TEMP_GLOBAL=20.0;
float OldTemp=-1;

/*Funci�n principal:
 * Se�alar que la tarea que escribe en LCD precisa m�s cantidad de memoria, por lo que
 * es la definici�n de la tarea, se multiplica por dos el valor de configMINIMAL_STACK_SIZE.
 * En las otras tareas se utiliza s�lo configMINIMAL_STACK_SIZE
 */

int main(void)
{

    // Inicializacio de mutexs
    xMutexI2C = xSemaphoreCreateMutex();
    xTomatemp=  xSemaphoreCreateMutex();

    // Comprueba si semaforo y mutex se han creado bien
    if ((xMutexI2C != NULL) && (xTomatemp!= NULL))
    {
        // Inicializacion del hardware (clocks, GPIOs, IRQs)
        prvSetupHardware();

        // Inicializaci�n de colas
                 InicializaColas();
        // Creacion de tareas
        xTaskCreate( prvHeartBeatTask, "Tarea 1:HeartBeatTask", configMINIMAL_STACK_SIZE, NULL,
                     HEARTBEAT_TASK_PRIORITY, NULL );
        xTaskCreate( prvTempLuxTask, "Tarea 2:TempLuxTask", configMINIMAL_STACK_SIZE, NULL,
                     TEMP_TASK_PRIORITY,NULL );
        // Creacion de tarea ObtenG
        xTaskCreate( prvObtenG,"Tarea 3:Aceler�metro", configMINIMAL_STACK_SIZE,
                    NULL,ACEL_TASK_PRIORITY, NULL );

      xTaskCreate( prvEscribeLCD, "Tarea 4:LCD", configMINIMAL_STACK_SIZE*2, NULL,
                    LCD_TASK_PRIORITY, NULL ); //Tenia dos opciones: aumentar configMINIMAL_STACK_SIZE
                                                // o multiplicar por dos, esta opci�n me resulta m�s eficiente.


        vTaskStartScheduler();

    }
    // Solo llega aqui si no hay suficiente memoria
    // para iniciar el scheduler
    return 0;
}

// Inicializacion del hardware del sistema

static void prvSetupHardware(void){

    extern void FPU_enableModule(void);

    // Configuracion del pin P1.0 - LED 1 - como salida y puesta en OFF
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);

    // Inicializacion de pins sobrantes para reducir consumo
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PE, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PE, PIN_ALL16);

    // Habilita la FPU
    MAP_FPU_enableModule();
    // Cambia el numero de "wait states" del controlador de Flash
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

    // Selecciona la frecuencia central de un rango de frecuencias del DCO
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_6);
    // Configura la frecuencia del DCO
    CS_setDCOFrequency(CS_8MHZ);

    // Inicializa los clocks HSMCLK, SMCLK, MCLK y ACLK
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    /* Inicializamos el display */
       Crystalfontz128x128_Init();

       /* Establecemos la orientaci�n de la pantalla*/
       Crystalfontz128x128_SetOrientation(LCD_ORIENTATION_UP);
       /* Initializes graphics context */
        Graphics_initContext(&g_sContext, &g_sCrystalfontz128x128, &g_sCrystalfontz128x128_funcs);
        Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED); //Fuente en color rojo
        Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_WHITE); // Fondo blanco
        GrContextFontSet(&g_sContext, &g_sFontFixed6x8);
        Graphics_clearDisplay(&g_sContext);
        Graphics_drawStringCentered(&g_sContext,
                                        (int8_t *)"Practica S. Empotrados:",
                                        AUTO_STRING_LENGTH,
                                        64,
                                        10,
                                        OPAQUE_TEXT);

    // Selecciona el nivel de tension del core
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE0);

    // Inicializacion del I2C
    I2C_init();
    // Inicializacion del sensor TMP006
    TMP006_init();
    // Inicializacion del sensor opt3001
    sensorOpt3001Init();
    sensorOpt3001Enable(true);



    //Prioridad para la interrupci�n ADC14
    MAP_Interrupt_setPriority(INT_ADC14, 0xA0);

    //Inicializa El Acelerometro
       init_Acelerometro(&Medida_Acc);
    // Habilita que el procesador responda a las interrupciones
       __enable_interrupt();
}

//Tarea heart beat
static void prvHeartBeatTask (void *pvParameters){

    for(;;){
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_ON_MS) );
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_OFF_MS) );

    }
}

//Tarea lectura temperatura y luminosidad
static void prvTempLuxTask (void *pvParameters){
    // Resultado del envio a la cola
    Queue_reg_t queueRegister;
    const TickType_t xTicksToWait = pdMS_TO_TICKS( 100 );
    float temperature;
    float convertedLux;
    uint16_t rawData;


    for(;;){
        vTaskDelay(pdMS_TO_TICKS(HALF_SECOND_MS));

          // Lee el valor de la Medida_Acc de luz
        sensorOpt3001Read(&rawData);
        sensorOpt3001Convert(rawData, &convertedLux);

        queueRegister.sensor = light;
        queueRegister.value = convertedLux;

           xQueueSend(xQueue_Temp_Lux,&queueRegister,xTicksToWait);

        vTaskDelay( pdMS_TO_TICKS(HALF_SECOND_MS) );  // Espera 500ms de la toma de luz m�s otros 500ms

        // Lee el valor de la Medida_Acc de temperatura
        temperature = TMP006_readAmbientTemperature();
        queueRegister.sensor = temp;
        queueRegister.value = temperature;
        if(xSemaphoreTake(xTomatemp,portMAX_DELAY)){
           TEMP_GLOBAL= temperature; //recogemos la temperatura para el ADC
           xSemaphoreGive(xTomatemp);
        }

        xQueueSend(xQueue_Temp_Lux,&queueRegister,xTicksToWait);


    }
}


/* Tarea que obtiene los datos del aceler�metro
 * Los encola una vez tratados en la funci�n lee_Acelerometro */
static void prvObtenG (void *pvParameters)
{
    float temperatura=0.0;
    uint8_t taketemp=0;  //Cada iteraci�n supone 100 ms, despu�s de 300 iteraciones habr� que tomar la temperatura
    const TickType_t xTicksToWait = pdMS_TO_TICKS( 100 );
    UBaseType_t uxHighWaterMark;

    for(;;){
        if(taketemp==0 && xSemaphoreTake(xTomatemp,portMAX_DELAY)){
                 temperatura= TEMP_GLOBAL; //recogemos la temperatura para el ADC despu�s de 300 iteraciones
                 taketemp=300;
                 xSemaphoreGive(xTomatemp);
         }
        lee_Acelerometro(&Medida_Acc, temperatura);

        xQueueSend(xQueue_Acel,&Medida_Acc,xTicksToWait);


        taketemp--;
        vTaskDelay(xTicksToWait);

    }
}
/* Tarea que recorre las dos colas y las manda al LCD */
/* Se ha intentado ocupar la menor memoria posible, de manera
 * que s�lo usa dos veces el tama�o m�nimo de stack.
 */
static void prvEscribeLCD (void *pvParameters)
{
    Queue_reg_t queueRegister;
    captura_A Lectura;
    const TickType_t xTicksToWait = pdMS_TO_TICKS( 100 );
    uint8_t contlux,conttemp;
    char message[50]="";
    char value[10];
    int i=0;
    uint8_t haydatos=0,haytemp=0;
    contlux=conttemp=0;
    valoresTemp=valorLux=0;
    valoresX=valoresY=valoresZ=0;
    UBaseType_t uxHighWaterMark;

    for(;;){

         for(i=BUFFER_SIZE;i>0;i--){

               if(xQueueReceive(xQueue_Temp_Lux,&queueRegister, 0 )==pdPASS)
               {

                  ftoa(queueRegister.value, value, 2);

                  if (queueRegister.sensor==light){
                      if(contlux<2){
                          valorLux+=queueRegister.value;
                          contlux++;
                      }
                      else{
                          MediaLux=valorLux/2;
                          contlux=0;
                          valorLux=0;
                          ftoa(MediaLux,value,2);
                          strcpy(message,"Lux: ");
                          strcat(message,value);
                              Graphics_drawStringCentered(&g_sContext,
                                                          (int8_t *)message,AUTO_STRING_LENGTH,64,30,OPAQUE_TEXT);
                          }



                    }else if(queueRegister.sensor==temp){

                          if(conttemp<60){
                              valoresTemp+= queueRegister.value;
                              if(xSemaphoreTake(xTomatemp,portMAX_DELAY)){
                                  TEMP_GLOBAL=queueRegister.value;
                              }
                              xSemaphoreGive(xTomatemp);
                              conttemp++;
                          }
                          else{
                              OldTemp=MediaTemp;
                              MediaTemp=valoresTemp/60;
                              valoresTemp=0;
                              conttemp=0;
                              ftoa(MediaTemp,value,2);
                              strcpy(message,"Temp: ");
                              strcat(message,value);
                              Graphics_drawStringCentered(&g_sContext,(int8_t *)message,AUTO_STRING_LENGTH,64,40,OPAQUE_TEXT);
                              GrFlush(&g_sContext);
                              if(haytemp){
                                  diferencia=MediaTemp-OldTemp;
                                  ftoa(diferencia,value,2);
                                  strcpy(message,"+-: ");
                                  strcat(message,value);

                                  Graphics_drawStringCentered(&g_sContext,
                                                             (int8_t *)message,AUTO_STRING_LENGTH,64,50,OPAQUE_TEXT);
                                  GrFlush(&g_sContext);
                              }
                              haytemp=1;
                         }
                  }
               } else{
                   i=0;
               }

          }// Fin del primer for*/

          for (i=BUFFER_SIZE;i>0;i--)
          {

                if(xQueueReceive(xQueue_Acel,&Lectura, 0 )==pdPASS)
                {

                  valoresX+=Lectura.EjeX;
                  valoresY+=Lectura.EjeY;
                  valoresZ+=Lectura.EjeZ;
                  haydatos++;

                  if(haydatos==10){
                     haydatos=0;
                     mediaX=valoresX/10;
                     ftoa(mediaX,value,3);
                     strcpy(message,"X: ");
                     strcat(message,value);
                     Graphics_drawStringCentered(&g_sContext,(int8_t *)message,AUTO_STRING_LENGTH,64,65,OPAQUE_TEXT);
                     GrFlush(&g_sContext);
                     mediaX=0;valoresX=0;
                     mediaY=0;
                     mediaY=valoresY/10;
                     ftoa(mediaY,value,3);
                     strcpy(message,"Y: ");
                     strcat(message,value);
                     Graphics_drawStringCentered(&g_sContext,(int8_t *)message,AUTO_STRING_LENGTH,64,75,OPAQUE_TEXT);
                     GrFlush(&g_sContext);
                     mediaY=0; valoresY=0;
                     mediaZ=valoresZ/10;
                     ftoa(mediaZ,value,3);
                     strcpy(message,"Z: ");
                     strcat(message,value);
                     Graphics_drawStringCentered(&g_sContext,(int8_t *)message,AUTO_STRING_LENGTH,64,85,OPAQUE_TEXT);
                     GrFlush(&g_sContext);
                     mediaZ=0;valoresZ=0;
                    }
                }

              else{
                  i=0;

              }
          }




    }
}


void InicializaColas(void){

    xQueue_Acel = xQueueCreate( 10, sizeof( captura_A ));
    xQueue_Temp_Lux=xQueueCreate( 10, sizeof( Queue_reg_t ));
    xQueueReset(xQueue_Acel);
    xQueueReset(xQueue_Temp_Lux);

}
