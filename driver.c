/*
 * driver.c
 *
 *  Created on: 18 nov. 2017
 *      Author: Antonio
 */

#include "driver.h"
#include "driverlib.h"
#include "adc14_multiple_channel_no_repeat.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <math.h>

/* Standard Includes */
#include <string.h>

#define Ov 0.03483038
#define Os -0.000532547

uint16_t resultsBuffer[NUM_ADC_CHANNELS];


captura_A Medidas;
SemaphoreHandle_t xBinarySemaphore;


/* Inicializar el acelerometro*/
// Se inicializa en sem�foro binario, el ADC y
void init_Acelerometro(captura_A * Medidas){
xBinarySemaphore=xSemaphoreCreateBinary();
init_ADC();
Medidas->EjeX=0;
Medidas->EjeY=0;
Medidas->EjeZ=0;

}

void lee_Acelerometro(captura_A *medida, float temperatura){

    float normaliza=0;
    float tog=0;
    float den=0;
    const TickType_t xMaxExpectedBlockTime = pdMS_TO_TICKS( 500 );
    uint16_t *rawData;

    rawData = ADC_read();
    if( xSemaphoreTake( xBinarySemaphore, xMaxExpectedBlockTime ) == pdPASS )
    {
        normaliza= (uint32_t) rawData[0] * 3.3f /16383;  //normalizamos el dato
        tog= (normaliza - 1.65 + Ov)/(0.66 * (1 + ((0.0001+Os) * temperatura))); //convertimos a G teniendo en cuenta temperatura
        medida->EjeX= tog + (0.7 * 0.001 * temperatura);
        normaliza= (uint32_t) rawData[1] * 3.3f /16383;  //normalizamos el dato
        tog= (normaliza - 1.65 + Ov)/(0.66 * (1 + ((0.0001+Os) * temperatura))); //convertimos a G teniendo en cuenta temperatura
        medida->EjeY= tog + (0.7 * 0.001 * temperatura);
        normaliza= (uint32_t) rawData[2] * 3.3f /16383;  //normalizamos el dato
        tog= (normaliza - 1.65 + Ov)/(0.66 * (1 + ((0.0004+Os) * temperatura))); //convertimos a G teniendo en cuenta temperatura
        medida->EjeZ= tog + (0.4 * 0.001 * temperatura);
    }
}






void MI_ADC14_IRQHandler(void)
{
    uint64_t status;
    static BaseType_t xHigherPriorityTaskWoken= pdFALSE;

    status = MAP_ADC14_getEnabledInterruptStatus();
    MAP_ADC14_clearInterruptFlag(status);

    if(status & ADC_INT2)
    {
        MAP_ADC14_getMultiSequenceResult(resultsBuffer);

        xSemaphoreGiveFromISR(xBinarySemaphore,&xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
    }

}
/* Funci�n float a cadena
// Funci�n que dado un n�mero, una cadena y
// un n�mero de decimales a considerar, devuelve
// en la cadena de caracteres la representaci�n
// del n�mero indicado.
void miftoa(float n, char *res, int decimales)
{
    char aux1[20]='\0';

    strcpy(aux1,res);
    // extraemos la parte entera
    int ipart = (int)n;

    // extraemos la parte decimal
    double fpart = n - (double)ipart;

    // convertimos la parte entera a cadena
    int i = MintToStr(ipart, res, 0);

    // Vemos si se consideran decimales
    if (decimales != 0)
    {
        res[i++] = '.';  // se a�ade el punto
        res[i]='\0';
        char aux[4]='\0';

        double multi=pow(10,decimales);
        fpart = fpart * multi;
        fpart=abs(fpart);
        MintToStr((int)fpart, aux, decimales);

        strcat(res,aux);
    }
    if(n<0)
    {
        aux1[0]='-';
        aux1[1]='\0';
        strcat(aux1,res);
        strcpy(res,aux1);
    }

}
*/

/* Funci�n entero a cadena
// Funci�n que dado un n�mero entero,
// devuelve su representaci�n en un cadena
// de caracteres.

int MintToStr(int x, char str[], int d)
{
    int i = 0;

    if (x!=0){

        x=abs(x);
        while (x)
        {
          str[i++] =(x%10)+ '0';
          x = x/10;
         }


        mireverse(str,i); //volvemos la cadena del rev�s
    }
    else
        str[i++]='0';
    str[i] = '\0';
    return i;
}
*/
/* Funci�n reverse
// Funci�n que devuelve la cadena opuesta
// a la que se le pasa como entrada.
void mireverse(char *str, int len)
{
    int i=0, j=len-1, temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}
*/
