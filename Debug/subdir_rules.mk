################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
adc14_multiple_channel_no_repeat.obj: ../adc14_multiple_channel_no_repeat.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source/third_party/CMSIS/Include" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="adc14_multiple_channel_no_repeat.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

driver.obj: ../driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source/third_party/CMSIS/Include" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="driver.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

i2c_driver.obj: ../i2c_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source/third_party/CMSIS/Include" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="i2c_driver.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

main.obj: ../main.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source/third_party/CMSIS/Include" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="main.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

msp432_startup_ccs.obj: ../msp432_startup_ccs.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source/third_party/CMSIS/Include" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="msp432_startup_ccs.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

system_msp432p401r.obj: ../system_msp432p401r.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source/third_party/CMSIS/Include" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12/source" --include_path="C:/ti/simplelink_msp432p4_sdk_1_50_00_12" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="system_msp432p401r.d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


