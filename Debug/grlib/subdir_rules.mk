################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
grlib/button.obj: ../grlib/button.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/button.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/checkbox.obj: ../grlib/checkbox.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/checkbox.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/circle.obj: ../grlib/circle.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/circle.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/context.obj: ../grlib/context.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/context.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/display.obj: ../grlib/display.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/display.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/image.obj: ../grlib/image.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/image.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/imageButton.obj: ../grlib/imageButton.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/imageButton.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/line.obj: ../grlib/line.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/line.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/radioButton.obj: ../grlib/radioButton.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/radioButton.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/rectangle.obj: ../grlib/rectangle.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/rectangle.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

grlib/string.obj: ../grlib/string.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/inc" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/grlib" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/LcdDriver" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/sensors" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/freertos/cortex-m4" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL/msp432" --include_path="D:/Users/Antonio/UOC/Eclipse launcher/workspace/PRAC_FINAL" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="grlib/string.d_raw" --obj_directory="grlib" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


